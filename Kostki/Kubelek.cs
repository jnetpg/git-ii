﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kostki
{
	class Kubelek
	{
		int IleKostek;
		int IleScian;

		Random rand = new Random(Guid.NewGuid().GetHashCode());

		public Kubelek(int ileKostek, int ileScian)
		{
			this.IleKostek = ileKostek;
			this.IleScian = ileScian;
		}

		public List<int> Rzut()
		{
			List<int> lista = new List<int>(IleKostek);
			for (int i = 0; i < this.IleKostek; i++)
			{
				lista.Add(rand.Next(0, IleScian));
			}
			return lista;
		}

	}
}
